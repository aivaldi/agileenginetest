import Dependencies._

ThisBuild / scalaVersion     := "2.12.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

resolvers += Resolver.url("bintray-sbt-plugins", url("http://dl.bintray.com/sbt/sbt-plugin-releases"))(Resolver.ivyStylePatterns)

mainClass in Compile := Some("html.Main")


lazy val root = (project in file("."))
  .settings(
    name := "task",
    libraryDependencies ++=  Seq(
    					scalaTest % Test,
						"ch.qos.logback" % "logback-classic" % "1.2.3" ,
						"com.typesafe.scala-logging" %% "scala-logging" % "3.9.2" ,
						"org.jsoup" % "jsoup" % "1.11.2"
					)
  )
