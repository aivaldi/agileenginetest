package comparator
import org.scalatest._
import html.Comparator
import com.typesafe.scalalogging.Logger

class ComparatorSpec extends FlatSpec with Matchers {
  "Compare Html" should "load with exception" in {
   
     implicit val log:Logger = Logger("main")
       val thrown = intercept[Exception] {
       Comparator.compareFiles("test_html/notExists.html", "test_html/sample-1-evil-gemini.html", "#make-everything-ok-button")
     }
     assert(thrown.isInstanceOf[IllegalArgumentException])
     
  }
  
  "Compare Html" should "Same files" in {
   
     implicit val log:Logger = Logger("main")
     val res = Comparator.compareFiles("test_html/sample-0-origin.html", "test_html/sample-0-origin-similar.html", "#make-everything-ok-button")
     assert(!res)
  }
  
  "Compare Html" should "load ok" in {
   
     implicit val log:Logger = Logger("main")
     val res = Comparator.compareFiles("test_html/sample-0-origin.html", "test_html/sample-1-evil-gemini.html", ":matchesOwn(Make everything OK)")
     assert(res, true)
  }
}
