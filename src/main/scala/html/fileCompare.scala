package html

import scala.io.Source

/**
 * Compare two HTML files 
 */
case class FileCompare()
object FileCompare {
  
  
  /**
   * Compare for file changes
   * Not too good algorithm must be improved
   */
  def fileCompare(file1:String, file2:String):Boolean = {
    
    val source = Source.fromFile(file1).getLines()
    val source2 = Source.fromFile(file2).getLines()
     
    source.sameElements(source2)
        
  }
  
  
}