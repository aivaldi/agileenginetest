package html

import java.nio.file.Files
import java.nio.file.Paths
import com.typesafe.scalalogging.Logger
import java.io.File
import org.jsoup.select.Elements
import scala.util.Success
import scala.util.Failure
import scala.collection.JavaConversions._

/**
 * Compare the diferent files
 */
case class Comparator()

object Comparator {
    
  /**
   * Check if the file exists
   * check if the files can be parsed as html
   * Returns boolean : True if comparation was made, false if not (equals files )
   */
  def compareFiles(originalFile:String, changedFile:String, selector:String)(implicit log:Logger):Boolean = {
    log.info("Comparator.compareFiles start")
    if ( !Files.isRegularFile( Paths.get(originalFile)) ||  
         ! Files.isRegularFile( Paths.get(changedFile)) ){
      log.error("Comparator.compareFiles error some files not exists")
      throw new IllegalArgumentException("Error loading files check if files exists")
    }
    log.info("Comparator.compareFiles end")
    
    if(FileCompare.fileCompare(originalFile, changedFile))
    {
      log.info("Comparator.compareFiles files are equals")
      println("Nothing to comapre, files are the same")
      return false
    }
    
    log.info("Comparator.compareFiles comparing files:")
    log.info(s"Comparator.compareFiles $originalFile")
    log.info(s"Comparator.compareFiles $changedFile")
    log.info(s"selector $selector")
      
    val f = new File(changedFile)
    
    
    HtmlFilder.findElementById( new File(changedFile), selector).map{
      els =>
        log.info(s"Comparator.compareFiles elements found ${els.size}")
        els.map{
          _.cssSelector()
       }
     
        
    }match {
    case Success(selector) => log.info("Target element/s: [{}]", selector)
    println("Selector(s) located at")
    selector.foreach(println)
    case Failure(ex) => {
      log.error("Error occurred.", ex)
      return false
      } 
  }
    
    return true
    
    
  }
  
}