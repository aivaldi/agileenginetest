package html

import java.io.File
import org.jsoup.Jsoup
import scala.util.Try
import scala.collection.JavaConverters._
import org.jsoup.nodes.Element

/**
 * This class wraps Jsoup to lookup the element expected
 */
case class HtmlFinder()
object HtmlFilder {

  val CHARSET_NAME = "utf8"

  /**
   * given a file looks for the selector
   * Multiple selector can be found
   * Exception is thorwn if jsoup cannot access to data
   */
  def findElementById(htmlFile: File, selector: String): Try[Iterable[Element]] = Try {
    Jsoup.parse(htmlFile, CHARSET_NAME, htmlFile.getAbsolutePath)
  }.map({
    case e=>
    e.select(selector).asScala
  })

}

