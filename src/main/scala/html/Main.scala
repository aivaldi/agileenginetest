package html

import com.typesafe.scalalogging._
/**
 * main class application
 * 
 * Load original file
 * Load other file to compare with
 * Optional selector default match any object with text 'Make everything OK'
 */
object Main extends App {
  implicit val logger = Logger("main")
  logger.info("Starting application")
  if (args.length < 2) {
    
        logger.info("Not enough arguments ")
        println("Two parameters are required ")
        println("""Example: sbt "run  <original file> <compare to file> (<selector>)" """)
        
   }else{
   
     val origin = args(0)
     val changed = args(1)
     var selector=":matchesOwn(Make everything OK)" 
     if ( args.length==3 )
       selector = args(2)
     
     try{
       Comparator.compareFiles(origin, changed, selector) 
     }catch{
       case e:Throwable => println(e.getMessage)
     }
     
   }
}